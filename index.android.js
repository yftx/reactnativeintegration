'use strict';
import codePush from "react-native-code-push";
import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  NativeModules,
  TextInput,
  Picker
} from 'react-native';
import RNCalculateModule from './src/native_module/calculate';
import {Button as RNButton} from 'react-native-elements';

class HelloWorld extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      first: "",
      second: "",
      symbol: 0,
      calculate_result: "",
      time: "no time"
    };
    this.symbolMap = ["+", "-", "*", "/"];
    this.calculate = this.calculate.bind(this);
    this.calculateAsync = this.calculateAsync.bind(this);
    this.getCurrentTime = this.getCurrentTime.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.hello}>RN插件实现</Text>
        <Button title="测试toast.show short" onPress={() => this.showToast("第一个测试")}/>
        <View style={styles.calculate_container}>
          <TextInput style={styles.num_input} keyboardType="numeric" onChangeText={(text) => this.setState({first: text})} placeholder="0" value={this.state.first}/>
          <Picker style={[
            styles.picker, {
              color: 'white',
              backgroundColor: '#333'
            }
          ]} mode="dropdown" selectedValue={this.state.symbol} onValueChange={(itemValue, itemPosition) => {
            this.setState({symbol: itemPosition});
            console.log(itemPosition, this.state.symbol);
            return;
          }}>
            <Picker.Item label={this.symbolMap[0]} color="red" value={0}/>
            <Picker.Item label={this.symbolMap[1]} color="green" value={1}/>
            <Picker.Item label={this.symbolMap[2]} color="blue" value={2}/>
            <Picker.Item label={this.symbolMap[3]} color="black" value={3}/>
          </Picker>
          <TextInput style={styles.num_input} keyboardType="numeric" onChangeText={(text) => this.setState({second: text})} placeholder="0" value={this.state.second}/>
          <Button title="等于" onPress={this.calculate}/>
          <Text>{this.state.calculate_result}</Text>
        </View>
        <View>
          <RNButton
            title="通过其他的activity获取当前时间"
            large
            backgroundColor='#397af8'
            onPress={this.getCurrentTime}
          />
          <Text>{this.state.time}</Text>
        </View>
      </View>
    )
  }

  showToast(msg) {
    NativeModules.RNToastModule.show(msg, NativeModules.RNToastModule.SHORT);
  }

  async calculateAsync() {
    return await RNCalculateModule.calculate(parseInt(this.state.first), parseInt(this.state.second), this.state.symbol);
  }

  calculate() {
    var promise = this.calculateAsync();
    promise.then((result) => {
      this.setState({calculate_result: result});
      console.log(result);
    }).catch((error) => {
      this.showToast(error.message)
    });
  }

  getCurrentTime() {
    var promise = async function() {
      return await NativeModules.RNListenActivityResultModule.getCurrentTime()
    }();
    promise.then((currentTime) => this.setState({time: currentTime}));
  }
}
var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around'
  },
  hello: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  calculate_container: {
    flexDirection: 'row'
  },
  num_input: {
    borderWidth: 1,
    width: 40,
    margin: 5
  },
  picker: {
    width: 100
  }
});

HelloWorld = codePush(HelloWorld);

AppRegistry.registerComponent('HelloWorld', () => HelloWorld);
