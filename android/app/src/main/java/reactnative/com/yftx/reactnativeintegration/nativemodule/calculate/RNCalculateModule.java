package reactnative.com.yftx.reactnativeintegration.nativemodule.calculate;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import reactnative.com.yftx.reactnativeintegration.nativemodule.RNModuleConstant;

/**
 * Created by yftx on 08/05/2017.
 */

public class RNCalculateModule extends ReactContextBaseJavaModule {

    private static final String PLUS_KEY = "plus";
    private static final String MINUS_KEY = "minus";
    private static final String MULTIPLICATION_KEY = "multiplication";
    private static final String DIVISION_KEY = "division";
    private static final int PLUS = 0;
    private static final int MINUS = 1;
    private static final int MULTIPLICATION = 2;
    private static final int DIVISION = 3;
//    ["+", "-", "*", "/"];

    public RNCalculateModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        Map<String, Object> parames = new HashMap<>();
        parames.put(PLUS_KEY, PLUS);
        parames.put(MINUS_KEY, MINUS);
        parames.put(MULTIPLICATION_KEY, MULTIPLICATION);
        parames.put(DIVISION_KEY, DIVISION);
        return parames;
    }

    @Override
    public String getName() {
        return RNModuleConstant.RNCalculateModuleName;
    }

    @ReactMethod
    public void calculate(int first, int second, int symbol, Promise promise) {
        try {

            double result = 0;
            switch (symbol) {
                case PLUS:
                    result = first + second;
                    break;
                case MINUS:
                    result = first - second;
                    break;
                case MULTIPLICATION:
                    result = first * second;
                    break;
                case DIVISION:
                    result = first / second;
                    break;
            }
            promise.resolve(result);
        } catch (Exception e) {
            promise.reject(e);
        }


    }
}
