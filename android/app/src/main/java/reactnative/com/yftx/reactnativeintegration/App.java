package reactnative.com.yftx.reactnativeintegration;

import android.app.Application;

import com.facebook.soloader.SoLoader;

/**
 * Created by yftx on 04/05/2017.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SoLoader.init(this, /* native exopackage */ false);
    }
}
