package reactnative.com.yftx.reactnativeintegration;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ReturnCurrentTimeActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String RESULT_KEY = "result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_int);
        findViewById(R.id.close_and_return).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra(RESULT_KEY, String.valueOf(System.currentTimeMillis()));
        setResult(RESULT_OK,intent);
        finish();
    }
}
