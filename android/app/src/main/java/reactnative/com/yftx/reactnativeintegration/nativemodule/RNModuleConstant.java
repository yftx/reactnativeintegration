package reactnative.com.yftx.reactnativeintegration.nativemodule;

/**
 * Created by yftx on 08/05/2017.
 */

public interface RNModuleConstant {
    public static String RNToastModuleName = "RNToastModule";
    public static String RNCalculateModuleName = "RNCalculateModule";
    public static String RNListenActivityResultModuleName = "RNListenActivityResultModule";
}
