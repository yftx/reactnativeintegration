# Copyright (c) 2015-present, Facebook, Inc.
# All rights reserved.
#
# This source code is licensed under the BSD-style license found in the
# LICENSE file in the root directory of this source tree. An additional grant
# of patent rights can be found in the PATENTS file in the same directory.

# 2048 is the max for non root users on Mac
# 修改socket数量,
# 对ulimit命令的介绍参考http://blog.csdn.net/whereismatrix/article/details/50582919
ulimit -n 2048
