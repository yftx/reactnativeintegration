主要包含如下几点.
1. 如何在已有项目中添加对RN的支持
2. 项目接入过程中的一些坑点
3. RN项目同Native项目之间通讯.
4. Native类容器ReactInstanceManager的使用.
5. bundle的打包及部署.
6. 热更新的方式.
7. RN的裁剪.

欢迎star,具体相关内容,请查看简书上的文章.

[参考我在简书上的文章](http://www.jianshu.com/p/0510af716d16)

